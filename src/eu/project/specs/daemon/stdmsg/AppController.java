package eu.project.specs.daemon.stdmsg;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.TimerTask;
import java.util.logging.Logger;

import eu.project.specs.daemon.stdmsg.Authenticate;

public class AppController extends TimerTask{
	
	private Input input;
	private Output output;
	
	Logger log = Logger.getLogger("Controller");
	
	public AppController(Input input, Output output) {
		this.input = input;
		this.output = output;
	}
	
    @Override
    public void run(){
    	InputStream authen = null;
    	
    	//System.out.println(input.authurl + input.authuser + input.authpass);
    	Authenticate auth = new Authenticate();
    	
		try {
			authen = auth.auth(input.authurl,input.authuser,input.authpass);
			output.outToFile(input.readUrlStream(authen));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
    }
}
