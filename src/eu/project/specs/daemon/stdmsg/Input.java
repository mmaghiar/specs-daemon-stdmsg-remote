package eu.project.specs.daemon.stdmsg;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Input {
	
	   public String authurl = null;
	   public String authuser = null;
	   public String authpass = null; 
	   public String outputfile = null;
	   public String refreshtimesec = null;
	   
	   public void readJsonConfig(String configFile){

		   JSONParser parser = new JSONParser();

		   try {
			   Object obj = parser.parse(new FileReader(configFile));
			   JSONObject jsonObject = (JSONObject) obj;
			   this.authurl = (String) jsonObject.get("authurl");
			   this.authuser = (String) jsonObject.get("authuser");
			   this.authpass = (String) jsonObject.get("authpass");
			   this.outputfile = (String) jsonObject.get("outputfile");
			   this.refreshtimesec = (String) jsonObject.get("refreshtimesec");
		   } catch (Exception e) {
			   e.printStackTrace();
		   }
	   }

	   public String readUrlStream(InputStream authen) throws IOException{
		   String line = "";
		   StringBuffer sb = new StringBuffer();
		   BufferedReader input = new BufferedReader(new InputStreamReader(authen) );
		   try {
			   while((line = input.readLine()) != null){
				   sb.append(line);
			   }
		   } catch (IOException e) {
			   e.printStackTrace();
		   }
		   input.close();
		   return sb.toString();
	   }
}
