package eu.project.specs.daemon.stdmsg;

import eu.project.specs.daemon.stdmsg.AppController;
import eu.project.specs.daemon.stdmsg.Input;
import eu.project.specs.daemon.stdmsg.Output;

import java.util.Timer;
import org.apache.commons.daemon.*;


public class AppDaemon implements Daemon{
	
	private static Timer timer = null;

	public static void main(String[] args) {
        //start the program
		
		Input input = new Input();		
		input.readJsonConfig("config.json");
		Output output = new Output(input.outputfile);
		Integer refreshIn;
        timer = new Timer();
        refreshIn = Integer.parseInt(input.refreshtimesec) * 1000;
        timer.schedule(new AppController(input, output), 0, refreshIn);
    }

    @Override
    public void init(DaemonContext dc) throws DaemonInitException, Exception {
        System.out.println("initializing ...");
    }
    @Override
    public void start() throws Exception {
        System.out.println("starting ...");
        main(null);
    }

    @Override
    public void stop() throws Exception {
        System.out.println("stopping ...");
        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    public void destroy() {
        System.out.println("done.");
    }

}
