package eu.project.specs.daemon.stdmsg;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Date;

public class Output {
	
	private String outFile;
	
	public Output(String file){
		this.outFile = file;
	}
	
	public void outToFile(String outData){
		
        File outFile = new File(this.outFile);
        
        try {
            FileOutputStream outFos = new FileOutputStream(outFile, true);
            PrintStream data = new PrintStream(outFos);
            data.println(new Date() + ": " + outData);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
	}
	
	
	
}
