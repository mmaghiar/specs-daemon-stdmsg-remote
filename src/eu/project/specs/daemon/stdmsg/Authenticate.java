package eu.project.specs.daemon.stdmsg;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Base64;


public class Authenticate {
	public InputStream auth(String address, String username, String password) throws Exception {
		  URL url = new URL(address);
		  URLConnection conn = url.openConnection();
		  conn.setConnectTimeout(30000); // 30 seconds time out
		 
		  if (username != null && password != null){
		    String user_pass = username + ":" + password;
		    String encoded = Base64.getEncoder().encodeToString(user_pass.getBytes("utf-8"));

		    conn.setRequestProperty("Authorization", "Basic " + encoded);
		  }
		  return conn.getInputStream();
		}
}